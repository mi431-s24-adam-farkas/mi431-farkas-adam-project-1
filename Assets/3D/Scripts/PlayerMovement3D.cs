using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement3D : MonoBehaviour
{
    [SerializeField]
    private float movementSpeed = 5;

    [SerializeField]
    private CharacterController controller;

    [SerializeField]
    private Camera playerCamera;

    [SerializeField]
    public float mouseSensitivity = 3.0f;

    private float verticalRotation = 0;

    private void Start()
    {
        Cursor.visible = false;
    }

    private void FixedUpdate()
    {


        // Rotation
        float rotX = Input.GetAxis("Mouse X") * mouseSensitivity;
        transform.Rotate(0, rotX, 0);

        verticalRotation -= Input.GetAxis("Mouse Y") * mouseSensitivity;
        verticalRotation = Mathf.Clamp(verticalRotation, -90, 90); // Limit vertical rotation angle
        playerCamera.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);

        // Movement
        float forwardSpeed = Input.GetAxis("Vertical") * movementSpeed;
        float sideSpeed = Input.GetAxis("Horizontal") * movementSpeed;

        // Calculate movement direction based on player's local rotation
        Vector3 speedVector = new Vector3(sideSpeed, 0, forwardSpeed);
        speedVector = transform.rotation * speedVector;

        // Apply gravity
        speedVector.y -= 100.81f * Time.deltaTime;

        // Move the player
        controller.Move(speedVector * Time.deltaTime);
    }
}
