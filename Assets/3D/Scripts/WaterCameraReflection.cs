using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterCameraReflection : MonoBehaviour
{

    [SerializeField]
    private Transform waterCameraTran;

    [SerializeField]
    private Transform player;

    [SerializeField]
    private Transform playerCameraTran;

    [SerializeField]
    private Transform waterSpriteTran;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        waterCameraTran.eulerAngles = new Vector3(waterCameraTran.eulerAngles.x, player.eulerAngles.y, waterCameraTran.eulerAngles.z);
    }
}
